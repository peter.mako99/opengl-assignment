#include <GL/glut.h>
#include <math.h>
#include <stdio.h>
#include <obj/load.h>
#include <obj/draw.h>

#include "world.h"

#define asteroids 1

void init_entities(World* world) {

    //Initialize Skybox Entity.
    init_entity(&(world->scene.skybox), 10000, 0, 0, 0, load_texture("textures\\sky.png"));

    //Initialize Black Hole entity.
    init_entity(&(world->scene.blackhole[0]), 1.7, 0, 0, 0, load_texture("textures\\bhole.png"));
    load_model(&world->scene.blackhole[0].model, "objects\\geoid.obj");

    //Initialize Black Hole's ring entity.
    init_entity(&(world->scene.blackhole[1]), 1.2, 0, 0, 0, load_texture("textures\\bhole.png"));
    load_model(&world->scene.blackhole[1].model, "objects\\geoid.obj");

    ////Initialize Asteroid entites.
    load_model(&world->scene.asteroid[0].model, "objects\\asteroid.obj");
    world->scene.asteroid[0].texture = load_texture("textures\\asteroid.png");
    init_asteroid_array();

    //Initialize PLanet Entity.
    init_entity(&(world->spaceobject.planet), 0.5, 4000, 2000, 0, load_texture("textures\\planet.png"));
    load_model(&world->spaceobject.planet.model, "objects\\geoid.obj");

    //Initialize Moon Entity.
    init_entity(&(world->spaceobject.moon), 0.1, 3700, 1700, 100, load_texture("textures\\moon.png"));
    load_model(&world->spaceobject.moon.model, "objects\\geoid.obj");

    //Initialize Eventhorizon Entity.
    init_entity(&(world->spaceobject.eventhorizon), 0.2, -20000, 0, 500, load_texture("textures\\ehorizon.png"));
    load_model(&world->spaceobject.eventhorizon.model, "objects\\ehorizon.obj");

    //Load Help screen texture.
    world->scene.help_texture = load_texture("textures\\help.png");
}

void init_asteroid_array() {
    int i;
    for (i = 0; i < 30; i++) {
        float x = (-150 + rand() % (150 + 1 - (-150)));
        float y = (1000 + rand() % (1200 + 1 - 1000)) * sin(i);
        float z = (1000 + rand() % (1200 + 1 - 1000)) * cos(i);

        init_entity(&(world.scene.asteroid[i]), 0.05, x, y, z, world.scene.asteroid[0].texture);
    }
}

void draw_environment(World world) {
    glEnable(GL_TEXTURE_2D);
    
    set_lights();
    list_setup();
    draw_skybox_bottom();
    draw_skybox_top();
    draw_black_hole();
    draw_geoids();
    draw_event_horizon();
    draw_asteroids();
}

void draw_skybox_bottom()
{
    double theta, phi1, phi2;
    double x1, y1, z1;
    double x2, y2, z2;
    double u, v1, v2;

    int n_slices, n_stacks;
    double radius;
    int i, k;

    n_slices = 12;
    n_stacks = 6;

    glPushMatrix();
    glBindTexture(GL_TEXTURE_2D, world.scene.skybox.texture);
    glScaled(world.scene.skybox.size, world.scene.skybox.size, world.scene.skybox.size);

    glColor3f(1, 1, 1);

    glBegin(GL_TRIANGLE_STRIP);

    for (i = 0; i < n_stacks; ++i) {
        v1 = (double)i / n_stacks;
        v2 = (double)(i + 1) / n_stacks;
        phi1 = v1 * M_PI / 2.0;
        phi2 = v2 * M_PI / 2.0;
        for (k = 0; k <= n_slices; ++k) {
            u = (double)k / n_slices;
            theta = u * 2.0 * M_PI;
            x1 = cos(theta) * cos(phi1);
            y1 = sin(theta) * cos(phi1);
            z1 = sin(phi1);
            x2 = cos(theta) * cos(phi2);
            y2 = sin(theta) * cos(phi2);
            z2 = sin(phi2);
            glTexCoord2d(u, 1.0 - v1);
            glVertex3d(x1, y1, -z1);
            glTexCoord2d(u, 1.0 - v2);
            glVertex3d(x2, y2, -z2);
        }
    }

    glEnd();
    glPopMatrix();
}

void draw_skybox_top()
{
    double theta, phi1, phi2;
    double x1, y1, z1;
    double x2, y2, z2;
    double u, v1, v2;

    int n_slices, n_stacks;
    double radius;
    int i, k;

    n_slices = 12;
    n_stacks = 6;

    glPushMatrix();
    glBindTexture(GL_TEXTURE_2D, world.scene.skybox.texture);
    glScaled(world.scene.skybox.size, world.scene.skybox.size, world.scene.skybox.size);

    glColor3f(1, 1, 1);

    glBegin(GL_TRIANGLE_STRIP);

    for (i = 0; i < n_stacks; ++i) {
        v1 = (double)i / n_stacks;
        v2 = (double)(i + 1) / n_stacks;
        phi1 = v1 * M_PI / 2.0;
        phi2 = v2 * M_PI / 2.0;
        for (k = 0; k <= n_slices; ++k) {
            u = (double)k / n_slices;
            theta = u * 2.0 * M_PI;
            x1 = cos(theta) * cos(phi1);
            y1 = sin(theta) * cos(phi1);
            z1 = sin(phi1);
            x2 = cos(theta) * cos(phi2);
            y2 = sin(theta) * cos(phi2);
            z2 = sin(phi2);
            glTexCoord2d(u, 1.0 - v1);
            glVertex3d(x1, y1, z1);
            glTexCoord2d(u, 1.0 - v2);
            glVertex3d(x2, y2, z2);
        }
    }

    glEnd();
    glPopMatrix();
}

void draw_black_hole() {

    glPushMatrix();
        glEnable(GL_BLEND);
        glTranslatef(world.scene.blackhole[0].position.x, world.scene.blackhole[0].position.y, world.scene.blackhole[0].position.z);
        glMaterialfv(GL_FRONT, GL_AMBIENT, world.scene.blackhole[0].material_ambient);
        glBindTexture(GL_TEXTURE_2D, world.scene.blackhole[0].texture);
        glScalef(world.scene.blackhole[0].size, world.scene.blackhole[0].size, world.scene.blackhole[0].size);
        draw_model(&world.scene.blackhole[0].model);

        //Draw the BlackHole's outer sphere

        glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);

        glColor4f(1, 1, 1, 0.15f);
        glTranslatef(world.scene.blackhole[1].position.x, world.scene.blackhole[1].position.y, world.scene.blackhole[1].position.z);
        glMaterialfv(GL_FRONT, GL_AMBIENT, world.scene.blackhole[1].material_ambient);
        glBindTexture(GL_TEXTURE_2D, world.scene.blackhole[1].texture);
        glScalef(world.scene.blackhole[1].size, world.scene.blackhole[1].size, world.scene.blackhole[1].size);
        draw_model(&world.scene.blackhole[1].model);
    glPopMatrix();
    glDisable(GL_BLEND);
}

void draw_asteroids() {
    int i;
    static double degree = 0;

    for (i = 0; i < 30; i++) {

        double angle = degree_to_radian(degree);

        world.scene.asteroid[i].position.y = cos(angle) * world.scene.asteroid[i].dist_from_origo.y - world.scene.asteroid[i].dist_from_origo.z * sin(angle);
        world.scene.asteroid[i].position.z = cos(angle) * world.scene.asteroid[i].dist_from_origo.z + world.scene.asteroid[i].dist_from_origo.y * sin(angle);

        glPushMatrix();
            glTranslatef(world.scene.asteroid[i].position.x, world.scene.asteroid[i].position.y, world.scene.asteroid[i].position.z);
            glCallList(asteroids);
        glPopMatrix();
    }
    degree += 0.1;
}

void draw_geoids() {
    
    glPushMatrix();
        glTranslatef(world.spaceobject.planet.position.x, world.spaceobject.planet.position.y, world.spaceobject.planet.position.z);
        glMaterialfv(GL_FRONT, GL_AMBIENT, world.spaceobject.planet.material_ambient);
        glBindTexture(GL_TEXTURE_2D, world.spaceobject.planet.texture);
        glScalef(world.spaceobject.planet.size, world.spaceobject.planet.size, world.spaceobject.planet.size);
        glRotatef(world.spaceobject.rotation, 0, 0, 1);
        draw_model(&world.spaceobject.planet.model);
    glPopMatrix();

    glPushMatrix();
        glTranslatef(world.spaceobject.moon.position.x, world.spaceobject.moon.position.y, world.spaceobject.moon.position.z);
        glMaterialfv(GL_FRONT, GL_AMBIENT, world.spaceobject.moon.material_ambient);
        glBindTexture(GL_TEXTURE_2D, world.spaceobject.moon.texture);
        glScalef(world.spaceobject.moon.size, world.spaceobject.moon.size, world.spaceobject.moon.size);
        glRotatef(world.spaceobject.rotation, 0, 0, 1);
        draw_model(&world.spaceobject.moon.model);
    glPopMatrix();
}

void draw_event_horizon() {
    glPushMatrix();
        glTranslatef(world.spaceobject.eventhorizon.position.x, world.spaceobject.eventhorizon.position.y, world.spaceobject.eventhorizon.position.z);
        glMaterialfv(GL_FRONT, GL_AMBIENT, world.spaceobject.eventhorizon.material_ambient);
        glBindTexture(GL_TEXTURE_2D, world.spaceobject.eventhorizon.texture);
        glScalef(world.spaceobject.eventhorizon.size, world.spaceobject.eventhorizon.size, world.spaceobject.eventhorizon.size);
        glRotatef(90, 0.0f, 0.0f, 1.0f);
        draw_model(&world.spaceobject.eventhorizon.model);
    glPopMatrix();
}

void list_setup() {
    glNewList(asteroids, GL_COMPILE);
        glMaterialfv(GL_FRONT, GL_AMBIENT, world.scene.asteroid[0].material_ambient);
        glBindTexture(GL_TEXTURE_2D, world.scene.asteroid[0].texture);
        glScalef(world.scene.asteroid[0].size, world.scene.asteroid[0].size, world.scene.asteroid[0].size);
        draw_model(&world.scene.asteroid[0].model);
    glEndList();
}

void set_lights() {
    
    float n = change_light_intensity(0);
    float light_position[] = { 0, 0, 0, 0 };
    float light_ambient[] = { n, n, n, 0 };
    float light_diffuse[] = { 0.5, 0.5, 0, 0 };
    float light_specular[] = { 1, 1, 1, 0 };

    glLightfv(GL_LIGHT1, GL_POSITION, light_position);
    glLightfv(GL_LIGHT1, GL_AMBIENT, light_ambient);
    glLightfv(GL_LIGHT1, GL_SPECULAR, light_specular);
    glLightfv(GL_LIGHT1, GL_DIFFUSE, light_diffuse);
}

float change_light_intensity(char i) {
    static float n = 0.5;

    switch (i) {
    case '+':
        if (n < 1);
        n += 0.01;
        break;
    case '-':
        if (n > -0.51);
        n -= 0.01;
        break;
    }

    return n;
}

void move_geoids() {
    static double degree = 0;

    if (macro.geoid_movement == TRUE) {
            degree += 0.1;
            double angle = degree_to_radian(degree);
            world.spaceobject.planet.dist_from_origo.x -= 2;
            world.spaceobject.planet.dist_from_origo.y -= 1;

            world.spaceobject.planet.position.x = cos(angle) * world.spaceobject.planet.dist_from_origo.x - world.spaceobject.planet.dist_from_origo.y * sin(angle);
            world.spaceobject.planet.position.y = cos(angle) * world.spaceobject.planet.dist_from_origo.y + world.spaceobject.planet.dist_from_origo.x * sin(angle);

            world.spaceobject.moon.position.x = world.spaceobject.planet.position.x - 300;
            world.spaceobject.moon.position.y = world.spaceobject.planet.position.y - 300;
    }
    if (world.spaceobject.planet.position.x == 0 && world.spaceobject.planet.position.y == 0) {
            
            world.spaceobject.planet.position.x = 4000;
            world.spaceobject.planet.position.y = 2000;

            world.spaceobject.moon.position.x = 3700;
            world.spaceobject.moon.position.y = 1700;

            world.spaceobject.planet.dist_from_origo.x = world.spaceobject.planet.position.x;
            world.spaceobject.planet.dist_from_origo.y = world.spaceobject.planet.position.y;
            degree = 0;
            macro.geoid_movement = FALSE;
    }
}

void rotate_geoids() {
    if (macro.geoid_rotation == TRUE) {
        world.spaceobject.rotation += 0.5;
    }
}

void move_eventhorizon() {
    static int eventhorizon = 0;
    if (macro.ehorizon_movement == TRUE) {
        if (eventhorizon == 0) {
            world.spaceobject.eventhorizon.position.x = -6000;
            world.spaceobject.eventhorizon.position.y = 1500;
        }
        world.spaceobject.eventhorizon.position.x += 15.0;
        eventhorizon = 1;
    }
    if (world.spaceobject.eventhorizon.position.x >= 6000) {
            world.spaceobject.eventhorizon.position.x = -20000;
            macro.ehorizon_movement = FALSE;
            eventhorizon = 0;
    }
}