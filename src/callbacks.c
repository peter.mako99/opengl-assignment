#include "callbacks.h"

#define VIEWPORT_RATIO (16.0 / 9.0)
#define VIEWPORT_ASPECT 50.0

struct {
	int x;
	int y;
} mouse_position;

void mouse_handler(int button, int state, int x, int y)
{
	mouse_position.x = x;
	mouse_position.y = y;
}

void motion_handler(int x, int y)
{
	double horizontal, vertical;

	horizontal = mouse_position.x - x;
	vertical = mouse_position.y - y;

	rotate_camera(&camera, horizontal, vertical);

	mouse_position.x = x;
	mouse_position.y = y;

	glutPostRedisplay();
}

void key_handler(unsigned char key, int x, int y)
{
		switch (key) {
		case 'w':
			set_camera_speed(&camera, 30);
			break;
		case 's':
			set_camera_speed(&camera, -30);
			break;
		case 'a':
			set_camera_side_speed(&camera, 30);
			break;
		case 'd':
			set_camera_side_speed(&camera, -30);
			break;
		case 32:
			set_camera_vertical_speed(&camera, 30);
			break;
		case 'c':
			set_camera_vertical_speed(&camera, -30);
			break;
		case 'q':
			if (macro.geoid_rotation == FALSE)
				macro.geoid_rotation = TRUE;
			else macro.geoid_rotation = FALSE;
			break;
		case 'e':
			if (macro.geoid_movement == FALSE)
				macro.geoid_movement = TRUE;
			else macro.geoid_movement = FALSE;
			break;
		case 'f':
			if (macro.ehorizon_movement == FALSE)
				macro.ehorizon_movement = TRUE;
			break;
		case '+':
			change_light_intensity('+');
			break;
		case '-':
			change_light_intensity('-');
			break;
		case 27:
			exit(0);
		}

	glutPostRedisplay();
}

void key_up_handler(unsigned char key, int x, int y)
{
	switch (key) {
	case 'w':
	case 's':
		set_camera_speed(&camera, 0.0);
		break;
	case 'a':
	case 'd':
		set_camera_side_speed(&camera, 0.0);
		break;
	case 32:
	case 'c':
		set_camera_vertical_speed(&camera, 0.0);
		break;
	case 27:
		exit(0);
	}
	

	glutPostRedisplay();
}

void special_key_handler(int key, int x, int y) {
	switch (key)
	{
	case GLUT_KEY_F1:
		if (world.scene.is_help_on)
		{
			world.scene.is_help_on = FALSE;
		}
		else {
			world.scene.is_help_on = TRUE;
		}
	}
}

void reshape(GLsizei width, GLsizei height) {

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	
	int x, y, w, h;
	double ratio;

	ratio = (double)width / height;
	if (ratio > VIEWPORT_RATIO) {
		w = (int)((double)height * (VIEWPORT_RATIO));
		h = height;
		x = (width - w) / 2;
		y = 0;
	}
	else {
		w = width;
		h = (int)((double)width / (VIEWPORT_RATIO));
		x = 0;
		y = (height - h) / 2;
	}

	glViewport(x, y, w, h);
	gluPerspective(VIEWPORT_ASPECT, 1.0 * w / h, 0.1, 20000.0);

	glMatrixMode(GL_MODELVIEW);
	
}

void display() {
	
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	
	set_view(&camera);
	draw_environment(world);
	
	if(world.scene.is_help_on) {
		show_help();
	}
	glutSwapBuffers();
}

void idle()
{
	update_camera(&camera);
	move_geoids();
	rotate_geoids();
	move_eventhorizon();
	glutPostRedisplay();
}
