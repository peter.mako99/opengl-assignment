#ifndef WORLD_H
#define WORLD_H

#include "texture.h"
#include "entity.h"

typedef struct SCENERY_STRUCT{
	int help_texture;
	int is_help_on;
	Entity blackhole[2];
	Entity skybox;
	Entity asteroid[30];
}Scenery;

typedef struct SPACEOBJECT_STRUCT {
	double rotation;
	Entity planet;
	Entity moon;
	Entity eventhorizon;
}Spaceobject;

typedef struct WORLD_STRUCT{
	Scenery scene;
	Spaceobject spaceobject;
}World;

World world;

//Initialize the entities of the world.
void init_entities(World* world);

//Draw the entities to the world.
void draw_environment(World world);

//Draw the top skybox.
void draw_skybox_top();

//Draw the bottom skybox.
void draw_skybox_bottom();

//Draw asteroid objects.
void draw_asteroids();

//Draw the geoid objects.
void draw_geoids();

//Draw "Event Horizon" object.
void draw_event_horizon();

//Draw the black hole object.
void draw_black_hole();

//Setup display lists used for drawing objects.
void list_setup();

//Set current lighting.
void set_lights();

//Change the intensity of lighting.
float change_light_intensity(char i);

//Initialize the whole asteroid entity array.
void init_asteroid_array();

//Move geoid objects towards the Black Hole.
void move_geoids();

//Move Event Horizon object across the universe.
void move_eventhorizon();

//Rotate geoid objects around their cores.
void rotate_geoids();

#endif /* WORLD_H */
