#ifndef UTILS_H
#define UTILS_H

#define FALSE 0
#define TRUE 1

//GLSL-like three dimensional vector
typedef struct vec3
{
	float x;
	float y;
	float z;
} vec3;

typedef struct MACRO_STRUCT {
	int geoid_movement;
	int geoid_rotation;
	int ehorizon_movement;
}Macro;

Macro macro;

//Converts degree to radian.
double degree_to_radian(double degree);

#endif /* UTILS_H */